package me.fulin.mywikipediagame.Utils;

import java.util.Random;

/**
 * Created by jack on 25/8/15.
 */
public class Utility {
    public static int randInt(int min, int max) {

        Random rand = new Random();

        int randomNum = rand.nextInt(max - min) + min;

        return randomNum;
    }
}
