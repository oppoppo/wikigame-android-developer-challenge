package me.fulin.mywikipediagame;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.ismaeltoe.FlowLayout;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

import me.fulin.mywikipediagame.modes.Paragraph;
import me.fulin.mywikipediagame.modes.Sentence;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getSimpleName();

    private FlowLayout mContainerId;
    private Button mButton;

    private String RANDOM_LIST_URL = "https://en.wikinews.org/w/api.php?action=query&list=random&format=json&rnnamespace=0&rnlimit=10";
    private String RANDOM_ARTICAL = "https://en.wikinews.org/w/api.php?action=parse&prop=text&format=json&pageid=";

    private int mSentenceNum = 0;
    private List<Paragraph> mParagraphs = new ArrayList<Paragraph>();
    private ArrayList<String> spinnerArray = new ArrayList<String>();
    private ArrayList<String> OrigSpinnerArray = new ArrayList<String>();
    private ArrayAdapter<String> adapter;
    private ArrayList<Spinner> spinnerControlArray = new ArrayList<>();

    private int Score = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        mContainerId = (FlowLayout) findViewById(R.id.containerView);
        mButton = (Button) findViewById(R.id.submitBt);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitAnswer();
            }
        });

        makeJsonObjectRequest();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void makeJsonObjectRequest() {

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArray);

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(RANDOM_LIST_URL, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    long pageId = response.getJSONObject("query").getJSONArray("random").getJSONObject(0).getLong("id");

                    JsonObjectRequest jsObjRequest2 = new JsonObjectRequest(RANDOM_ARTICAL + pageId, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            parseJson(response);
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d("error", error.toString());
                        }
                    });

                    AppController.getInstance().addToRequestQueue(jsObjRequest2);
                } catch (Exception e) {
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        AppController.getInstance().addToRequestQueue(jsObjRequest);

    }

    private void parseJson(JSONObject response) {
        try {

            String text = response.getJSONObject("parse").getJSONObject("text").getString("*");

            Document doc = Jsoup.parse(text);
            Elements pElements = doc.getElementsByTag("p");

            for (int i = 1; i < pElements.size(); i++) {
                Paragraph tmpParagraph = new Paragraph();

                final Pattern p = Pattern.compile("[\\.\\!\\?]\\s+", Pattern.MULTILINE);
                String[] sentences = p.split(pElements.get(i).text());
                for (int j = 0; j < sentences.length; j++) {
                    mSentenceNum++;
                    tmpParagraph.mSentences.add(new Sentence(sentences[j]));
                }

                if (sentences.length > 0) {
                    mParagraphs.add(tmpParagraph);
                }

                if (mSentenceNum >= 10) {
                    break;
                }
            }

            updateUi();
        } catch (Exception e) {

        }
    }

    private void updateUi() {
        //spinnerArray.add(" ");

        for (Paragraph p : mParagraphs) {
            for (Sentence s : p.mSentences) {
                if (s.getSentence().length() == 0) continue;

                TextView startSentence = new TextView(MainActivity.this);
                String t1String = s.getSentence().substring(0, s.getIndex());

                startSentence.setText(t1String);

                mContainerId.addView(startSentence,
                        new FlowLayout.LayoutParams(
                                FlowLayout.LayoutParams.WRAP_CONTENT,
                                FlowLayout.LayoutParams.WRAP_CONTENT));


                spinnerArray.add(s.getRemoveWord());

                Spinner blankWord = new Spinner(MainActivity.this);
                blankWord.setAdapter(adapter);
                spinnerControlArray.add(blankWord);

                mContainerId.addView(blankWord,
                        new FlowLayout.LayoutParams(
                                FlowLayout.LayoutParams.WRAP_CONTENT,
                                FlowLayout.LayoutParams.WRAP_CONTENT));

                TextView endSentence = new TextView(MainActivity.this);
                int length = s.getRemoveWord().length();

                endSentence.setText(s.getSentence().substring(s.getIndex() + length));
                mContainerId.addView(endSentence,
                        new FlowLayout.LayoutParams(
                                FlowLayout.LayoutParams.WRAP_CONTENT,
                                FlowLayout.LayoutParams.WRAP_CONTENT));


            }
        }

        //randomSpinnerArray.addAll(spinnerArray);
        OrigSpinnerArray = (ArrayList<String>) spinnerArray.clone();
        //Collections.copy(spinnerArray, randomSpinnerArray);
        Collections.shuffle(spinnerArray);
        spinnerArray.add(0, " ");
    }

    public void restartActivity() {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }

    private void submitAnswer() {
        Score = 0;

        Log.d("***OrigSpinnerArray**", OrigSpinnerArray.size() + " ");
        for (int i = 0; i < spinnerControlArray.size(); i++) {
            if (OrigSpinnerArray.get(i).equals(spinnerControlArray.get(i).getSelectedItem().toString())) {
                Score++;
            }
        }

        new AlertDialog.Builder(MainActivity.this)
                .setTitle("Your Score")
                .setMessage(" " + Score)
                .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //do nothing - it will close on its own
                    }
                })
                .setPositiveButton("Replay", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        restartActivity();
                    }
                })
                .show();
    }

}
