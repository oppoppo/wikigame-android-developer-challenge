package me.fulin.mywikipediagame.modes;

import me.fulin.mywikipediagame.Utils.Utility;

/**
 * Created by jack on 25/8/15.
 */
public class Sentence {
    private String mSentence;
    private int mIndex;
    private String mRemoveWord;
    private String mAnswerWord;
    private String[] words;

    public Sentence(String sentence)
    {
        this.mSentence = sentence;

        words = mSentence.split("\\s+");

        this.mRemoveWord = words[Utility.randInt(0, words.length)];

        mIndex = mSentence.indexOf(mRemoveWord);

    }

    public String getSentence() {
        return mSentence;
    }

    public void setSentence(String mSentence) {
        this.mSentence = mSentence;
    }

    public int getIndex() {
        return mIndex;
    }

    public void setIndex(int mIndex) {
        this.mIndex = mIndex;
    }

    public String getRemoveWord() {
        return mRemoveWord;
    }

    public void setRemoveWord(String mRemoveWord) {
        this.mRemoveWord = mRemoveWord;
    }

    public String getAnswerWord() {
        return mAnswerWord;
    }

    public void setAnswerWord(String mAnswerWord) {
        this.mAnswerWord = mAnswerWord;
    }

    public String[] getWords() {
        return words;
    }

    public void setWords(String[] words) {
        this.words = words;
    }
}
